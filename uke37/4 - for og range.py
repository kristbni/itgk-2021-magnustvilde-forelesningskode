'''mer for-løkke'''

# lag en for-løkke som skriver ut tallene 5-9
for i in range(5,10):
    print(f'i: {i}')

# lag en for-løkke som skriver ut partallene mellom 0 og 10
for j in range (2,10,2):
    print(f'j: {j}')

# lag en for-løkke som skriver ut tallene 1-5 i motsatt rekkefølge

for number in range(5,0,-1):
    print(f'number: {number}')

